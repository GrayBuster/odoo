//
//  LoginControllerViewController.swift
//  Odoo
//
//  Created by gray buster on 9/12/18.
//  Copyright © 2018 gray buster. All rights reserved.
//

import UIKit
import Alamofire

class LoginController: UIViewController {
    
    var user: User?
    
    let api = API()
    
    @IBOutlet var spinner: UIActivityIndicatorView!
    
    @IBOutlet var loadingView: UIView! 
    
    @IBOutlet weak var usernameTextField: UITextField! {
        didSet {
            usernameTextField.addBottomBorderWithColor(UIColor(r: 195, g: 171, b: 120), width: 1)
        }
    }
    
    @IBOutlet weak var passwordTextField: UITextField! {
        didSet {
            passwordTextField.addBottomBorderWithColor(UIColor(r: 195, g: 171, b: 120), width: 1)
        }
    }
    
    @IBOutlet weak var signupBtn: UIButton! {
        didSet {
            if let title = signupBtn.titleLabel?.text {
                signupBtn.setAttributedTitle(title.getUnderLineAttributedText(), for: .normal)
            }
        }
    }
    
    @IBOutlet weak var loginBtn: UIButton!
    @IBOutlet weak var checkBox: CheckBox!
    
    //Button clicked functions
    @IBAction func onClickedBtn(_ sender: UIButton) {
        switch sender {
        case loginBtn:
            guard let username = usernameTextField.text, let password = passwordTextField.text else { return }
            if !username.isEmpty && !password.isEmpty {
                self.view.addSubview(loadingView)
                self.view.isUserInteractionEnabled = false
                login(with: username, password)
            }
        default:
            break
        }
    }
    //Login with username and password
    func login(with username: String,_ password: String) {
        Parameter.params = [
            "db":"hhd_e300_vn_test01",
            "login": username,
            "password": password
        ]
        api.callJsonRpc(args: Parameter.params, function: Function.Login, success: { json in
            self.user = User(from: json)
            DispatchQueue.main.async {
                self.loadingView.removeFromSuperview()
                self.performSegue(withIdentifier: "Login Successfully", sender: nil)
                
            }
        }) { error in
            print("login failed with error: ",error)
            DispatchQueue.main.async {
                self.showAlert(withTitle: "Cảnh báo", message: "Bạn không có đường truyền internet!", handler: nil)
                self.loadingView.removeFromSuperview()
                self.view.isUserInteractionEnabled = true
            }
        }
    }
    
    @objc func dismisKeyboard() {
        view.endEditing(true)
    }
    
    
    // MARK: - Controller LifeCycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        spinner.center = view.center
        loadingView.center = view.center
        let tapGesuter = UITapGestureRecognizer(target: self, action: #selector(dismisKeyboard))
        view.addGestureRecognizer(tapGesuter)
        loadingView.center = view.center
        loadingView.layer.cornerRadius = 25
        
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if !NetworkState.isConnected() {
            self.showAlert(withTitle: "Cảnh báo!", message: "Bạn đang hiện không có internet!", handler: nil)
        }
    }

 
    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "Login Successfully" {
            if let containerListController = segue.destination.content as? ContainerListController {
                containerListController.user = self.user
            }
        }
    }

}
