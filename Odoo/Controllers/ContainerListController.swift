//
//  ContainerListController.swift
//  Odoo
//
//  Created by gray buster on 9/13/18.
//  Copyright © 2018 gray buster. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import FontAwesome_swift

protocol HandleStuffDelegate {
    func didPressSearchButton(with searchText: String)
    func didPressCancelSearchBar()
    func didTapSort(withType type: SortType)
    func didTapNew()
    func didChooseStock(type: StockPickingType)
    func didChoose(_ filter: Filter)
}

enum SortType: String {
    case asc
    case desc
}

enum StockPickingType:String {
    case incoming
    case outgoing
    case inter = "internal"
}

class ContainerListController: UIViewController,HandleFilterDelegate {
    
    
    func didChooseType(of filter: Filter) {
//        let stockpickingController = storyboard?.instantiateViewController(withIdentifier: "StockPicking Controller") as! StockPickingController
//        stockpickingController.filter = filter
        delegate?.didChoose(filter)
    }
      
    var delegate: HandleStuffDelegate?
    
    var user: User?
    
    private var sortDown = false
    
    private var timer: Timer?
    
    @IBOutlet var searchBar: UISearchBar! {
        didSet {
            //Change color 's title of search bar
            self.searchBar.tintColor = UIColor.white
            searchBar.setValue("Huỷ", forKey:"_cancelButtonText")
        }
    }
    
    @IBOutlet var searchBtn: UIBarButtonItem! {
        didSet {
            //Change Font with search icon
            searchBtn.title = ""
            searchBtn.change(fontName: "Font Awesome 5 Free", withFontSize: 24, for: .normal)
            searchBtn.change(fontName: "Font Awesome 5 Free", withFontSize: 24, for: .highlighted)
        }
    }
    
    @IBOutlet var settingBtn: UIBarButtonItem! {
        didSet {
            //Change Font with setting icon
            settingBtn.title = ""
            settingBtn.change(fontName: "Font Awesome 5 Free", withFontSize: 24, for: .normal)
            settingBtn.change(fontName: "Font Awesome 5 Free", withFontSize: 24, for: .highlighted)
        }
    }
  
    @IBOutlet var menuBtn: UIBarButtonItem! {
        didSet {
            //Change Font with hamburger icon
            menuBtn.title = ""
            menuBtn.change(fontName: "Font Awesome 5 Free", withFontSize: 24, for: .normal)
            menuBtn.change(fontName: "Font Awesome 5 Free", withFontSize: 24, for: .highlighted)
        }
    }
    
    
    @IBOutlet weak var bottomTabBar: UITabBar! {
        didSet {
            //Change Font with hamburger icon
            setupBarItem(withFontSize: 15, ofImageWidth: 25, height: 25)
            bottomTabBar.change(size: 13, weight: .medium, underlineColor: nil, fontColor: .gray, fontSelectedColor: .white, lineWidth: nil)
            coloredSelectedTab()
            bottomTabBar.tintColor = .white
            bottomTabBar.selectedItem = bottomTabBar.items?.last
        }
    }
    
    @IBOutlet weak var titleView: UIView! {
        didSet {
            titleView.addBottomBorderWithColor( UIColor(r: 224, g: 224, b: 224), width: 1)
        }
    }
    
    @IBOutlet weak var topTabBar: UITabBar! {
        didSet {
            topTabBar.selectedItem = topTabBar.items![0]
            topTabBar.change(size: 15, weight: .medium, underlineColor: UIColor(r: 251, g: 153, b: 140), fontColor: .black, fontSelectedColor: .black, lineWidth: 4)
            topTabBar.addBottomBorderWithColor( UIColor(r: 224, g: 224, b: 224), width: 1)
        }
    }
    
    //Show and hide search bar
    @IBAction func hideAndShowSearchBar(_ sender: UIBarButtonItem) {
        UIView.transition(with: view, duration: 0.3, options: .transitionCrossDissolve, animations: {
            if self.navigationItem.titleView == nil {
                self.navigationItem.titleView = self.searchBar
                self.searchBar.becomeFirstResponder()
                self.navigationItem.leftBarButtonItem = nil
                self.navigationItem.rightBarButtonItems = nil
            }
        })
    }
    
    //Colored Selected tab
    private func coloredSelectedTab() {
        let numberOfItems = CGFloat((bottomTabBar.items!.count))
        let tabBarItemSize = CGSize(width: bottomTabBar.frame.width / numberOfItems, height: bottomTabBar.frame.height)
        bottomTabBar.selectionIndicatorImage = UIImage.imageWithColor(color: UIColor(r: 251, g: 153, b: 140), size: tabBarItemSize).resizableImage(withCapInsets: UIEdgeInsets.zero)
    }
    
    private func setupBarItem(withFontSize size: CGFloat,ofImageWidth width: CGFloat,height: CGFloat) {
        //New item bar
        let newImage = UIImage.fontAwesomeIcon(name: .plus, style: .solid, textColor: .gray, size: CGSize(width: width, height: height))
        bottomTabBar.items![2].image = newImage
        //Sort item bar
        let sortImage = UIImage.fontAwesomeIcon(name: .sortAmountDown, style: .solid, textColor: .gray, size: CGSize(width: width, height: height))
        bottomTabBar.items![1].image = sortImage
        //Filter item bar
        let filterImage = UIImage.fontAwesomeIcon(name: .filter, style: .solid, textColor: .gray, size: CGSize(width: width, height: height))
        bottomTabBar.items![0].image = filterImage
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "Send" {
            if let stockPickingController = segue.destination.content as? StockPickingController {
                stockPickingController.user = self.user
                delegate = stockPickingController
            }
        }else if segue.identifier == "Filter" {
            if let filterController = segue.destination.content as? FilterController {
                filterController.delegate = self
            }
        }
    }
}

// MARK: Tab bar delegate
extension ContainerListController: UITabBarDelegate {
    
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        if let textTitleBarItem = item.title {
            handleFunctionalTabBar(with: textTitleBarItem)
        }
    }
    //Tab bar functions
    func handleFunctionalTabBar(with tilteBar: String) {
        switch tilteBar {
        case "Sort":
            if sortDown {
                delegate?.didTapSort(withType: .desc)
                let sortDownImage = UIImage.fontAwesomeIcon(name: .sortAmountDown, style: .solid, textColor: .gray, size: CGSize(width: 25, height: 25))
                bottomTabBar.items![1].selectedImage = sortDownImage
                bottomTabBar.items![1].image = sortDownImage
            } else {
                delegate?.didTapSort(withType: .asc)
                bottomTabBar.items![1].selectedImage = #imageLiteral(resourceName: "sort up icon")
                bottomTabBar.items![1].image = #imageLiteral(resourceName: "sort up icon")
            }
            sortDown = !sortDown
        case "New":
            break
        case "Filter":
            self.performSegue(withIdentifier: "Filter", sender: nil)
        case "Xuất Kho":
            delegate?.didChooseStock(type: .outgoing)
        case "Nhập kho":
            delegate?.didChooseStock(type: .incoming)
        case "Chuyển kho":
            delegate?.didChooseStock(type: .inter)
        default:
            break
        }

    }
}


// MARK: Search Bar Delegate

extension ContainerListController: UISearchBarDelegate {
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.text = nil
        delegate?.didPressCancelSearchBar()
        UIView.transition(with: view, duration: 0.3, options: .transitionCrossDissolve, animations: {
            if self.navigationItem.titleView == nil {
                self.navigationItem.titleView = self.searchBar
                self.navigationItem.leftBarButtonItem = nil
                self.navigationItem.rightBarButtonItems = nil
            } else {
                self.navigationItem.titleView?.removeFromSuperview()
                self.navigationItem.titleView = nil
                self.navigationItem.leftBarButtonItem = self.menuBtn
                self.navigationItem.rightBarButtonItems = [self.settingBtn,self.searchBtn]
            }
        })
        searchBar.resignFirstResponder()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if !searchText.isEmpty {
            if searchText.count > 1 {
                timer?.invalidate()
                timer = Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(delaySearch(_:)), userInfo: searchText, repeats: false)
            }
        }
    }
    
    @objc func delaySearch(_ timer: Timer) {
        let searchText = timer.userInfo as! String
        delegate?.didPressSearchButton(with: searchText)
        timer.invalidate()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        guard let searchText = searchBar.text else { return }
        delegate?.didPressSearchButton(with: searchText)
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(true, animated: true)
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(false, animated: true)
        
    }
}





