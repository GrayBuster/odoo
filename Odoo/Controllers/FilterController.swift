//
//  FilterController.swift
//  Odoo
//
//  Created by gray buster on 9/18/18.
//  Copyright © 2018 gray buster. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

protocol HandleFilterDelegate {
    func didChooseType(of filter: Filter)
}

class FilterController: UIViewController {
    
    
    private var xmlParser: XMLParser?
    
    
    var filters = [Filter]()
    
    var delegate: HandleFilterDelegate?
    
    @IBOutlet weak var dismisBtn: UIButton!
    
    @IBOutlet weak var applyBtn: UIButton!
    
    @IBOutlet weak var removeBtn: UIButton!
    
    @IBOutlet weak var checkBoxActivity: CheckBox!
    
    @IBOutlet weak var checkBoxAction: CheckBox!
    
    @IBOutlet weak var filterSearchBar: UISearchBar!
    
    @IBOutlet weak var filterTableView: UITableView!
    
    @IBOutlet weak var containerView: UIView! {
        didSet {
            containerView.addTopBorderToView(UIColor(r: 229, g: 230, b: 232), lineWidth: 1)
        }
    }
    
    @IBAction func onClickedBtn(_ sender: UIButton) {
        switch sender {
        case dismisBtn:
            self.dismiss(animated: true, completion: nil)
        default:
            break
        }
    }
    
    private func callFilterAPI() {
        let filter = Filter()
        Parameter.domain = [["model", "=", "stock.picking"], ["type", "=", "search"]]
        Parameter.fields = ["arch_base"]
        Parameter.kwargs = [:]
        Parameter.context = [:]
        filter.search_read(domain: Parameter.domain, fields: Parameter.fields, offset: 0, limit: 10, order: "", context: Parameter.context, success: { json in
            if json != JSON.null , json.count > 0 {
                for i in json {
                    //Get xml from string
                    let str = i.1["arch_base"].stringValue
                    //Parse xml from string encode utf8
                    if let data = str.data(using: String.Encoding.utf8)  {
                        self.xmlParser = XMLParser(data: data)
                        self.xmlParser!.delegate = self
                        self.xmlParser!.parse()
                    }
                }
                
            }
        }) { error in
            print(error)
        }
    }
    
    // MARK: Controller LifeCylce
    
    override func viewDidLoad() {
        super.viewDidLoad()
        callFilterAPI()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        filterSearchBar.changePositionToRightOfSearchImageIcon(withWidth: 25, height: 25, color: UIColor(r: 143, g: 141, b: 141))
    }
}


//MARK : UITableView Datasource & Delegate
extension FilterController: UITableViewDelegate,UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filters.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FilterCell", for: indexPath) as! FilterCell
        if let name = filters[indexPath.row].name {
            cell.contentLabel.text = name
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let filter = filters[indexPath.row]
        self.delegate!.didChooseType(of: filter)
        self.dismiss(animated: true) {
            
        }
        
    }
    
    
}

//MARK: XML Parser Delegate
extension FilterController: XMLParserDelegate {
    
    func parser(_ parser: XMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
    }
    
    func parser(_ parser: XMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String] = [:]) {
        if let name = attributeDict["string"] , let domain = attributeDict["domain"] {
            if domain.count > 2 {
                let filter = Filter()
                filters.append(filter.getObject(withName: name, domains: extractTuple(domain)))
            }
        }
        
    }
    
    func parserDidEndDocument(_ parser: XMLParser) {
        if filters.count > 0 {
            filterTableView.reloadData()
        }
    }
    
    func extractTuple(_ string: String) -> [Any] {
        // Removes [ and ( and ' and ) and ] from the string
        let pureValue = string.replacingOccurrences(of: "[", with: "", options: .caseInsensitive, range: nil).replacingOccurrences(of: "(", with: "", options: .caseInsensitive, range: nil).replacingOccurrences(of: "]", with: "", options: .caseInsensitive, range: nil).replacingOccurrences(of: "'", with: "", options: .caseInsensitive, range: nil).replacingOccurrences(of: "]", with: "", options: .caseInsensitive, range: nil).replacingOccurrences(of: ")", with: "", options: .caseInsensitive, range: nil).replacingOccurrences(of: " ", with: "", options: .caseInsensitive, range: nil)
        // Split string to array of string by ","
        var pureArray = pureValue.components(separatedBy: ",")
        
        //Return array
        var finalArr = [Any]()
        //
        var contentArr = [Any]()
        var stateArr = [Any]()
        
        
        if pureArray.count > 3 {
            // Append the rest elements of array where element = "in"
            for i in 0...pureArray.count-1 {
                if pureArray[i] == "in" {
                    let k = i+1
                    for j in k..<pureArray.count {
                        contentArr.append(pureArray[j])
                    }
                    //Append element "in" to array
                    stateArr.append(pureArray[i])
                    if stateArr.count > 2 {
                        continue
                    }
                }else if pureArray[i] == "state" {
                    //Append element "state" to array
                    stateArr.append(pureArray[i])
                }
            }
            //Append array of contentArr to stateArr
            stateArr.append(contentArr)
            //Append elements 's stateArr to finalArr
            stateArr.forEach { item in
                finalArr.append(item)
            }
        }else {
            //Cast if there is true or false in sequence
            pureArray.forEach { item in
                switch item {
                case "True":
                    finalArr.append(true)
                case "False":
                    finalArr.append(false)
                default:
                    finalArr.append(item)
                }
            }
        }
        return finalArr
    }
}





