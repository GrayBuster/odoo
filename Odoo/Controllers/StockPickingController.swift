//
//  PhieuNhapTableController.swift
//  Odoo
//
//  Created by gray buster on 9/13/18.
//  Copyright © 2018 gray buster. All rights reserved.
//

import UIKit
import SwiftyJSON

class StockPickingController: UITableViewController,HandleStuffDelegate {
    
    //MARK: - HandleStuff Delegate
    
    //Handle filter stock picking
    func didChoose(_ filter: Filter) {
        if let domains = filter.domains {
            self.stockPickings.removeAll()
            self.filter = true
            self.filterType = domains
            self.tableView.reloadData()
            if sort {
                let order = "name " + sortType!.rawValue
                self.setupStockPicking(withType: stockpickingType!, filters: domains, by: order)
            } else {
                self.setupStockPicking(withType: stockpickingType!, filters: domains)
            }
            
            print(domains)
        }
    }
    
    //Handle cancel button
    func didPressCancelSearchBar() {
        searchResult.removeAll()
        searchActive = false
        self.tableView.reloadData()
        let indexPath = IndexPath(item: 0, section: 0)
        self.tableView.scrollToRow(at: indexPath, at: .top, animated: true)
    }
    
    //Handle search
    func didPressSearchButton(with searchText: String) {
        searchActive = true
        self.searchText = searchText
        searchProject(by: stockpickingType!, with: searchText)
    }
    
    //Handle sort tab
    func didTapSort(withType type: SortType) {
        sort = true
        self.sortType = type
        var order = "name "
        switch type {
        case .asc:
            self.sortResult.removeAll()
            order += type.rawValue
        case .desc:
            self.sortResult.removeAll()
            order += type.rawValue
        }
        setupStockPicking(withType: stockpickingType!, by: order)
        //self.tableView.setContentOffset( CGPoint(x: 0, y: -tableView.contentInset.top) , animated: true)
    }
    
    //Handle Load StockPicking type
    func didChooseStock(type: StockPickingType) {
        self.stockpickingType = type
        sortResult.removeAll()
        stockPickings.removeAll()
        self.tableView.separatorStyle = .none
        self.tableView.reloadData()
        number = 0
        if sort && filter {
            let order = "name " + self.sortType!.rawValue
            setupStockPicking(withType: type,filters: filterType, by: order)
            if sortResult.count > 0 {
                self.tableView.setContentOffset( CGPoint(x: 0, y: -tableView.contentInset.top) , animated: true)
            }
        } else if sort {
            let order = "name " + self.sortType!.rawValue
            setupStockPicking(withType: type,filters: filterType, by: order)
            if sortResult.count > 0 {
                self.tableView.setContentOffset( CGPoint(x: 0, y: -tableView.contentInset.top) , animated: true)
            }
        } else if filter {
            setupStockPicking(withType: type,filters: filterType)
        } else {
            setupStockPicking(withType: type)
            self.tableView.setContentOffset( CGPoint(x: 0, y: -tableView.contentInset.top) , animated: true)
//            let indexPath = IndexPath(item: 0, section: 0)
//            self.tableView.scrollToRow(at: indexPath, at: .top, animated: true)
        }
        
    }
    
    //Handle press new tab
    func didTapNew() {
        sort = false
        searchActive = false
        searchResult.removeAll()
        sortResult.removeAll()
        self.tableView.reloadData()
        let indexPath = IndexPath(item: 0, section: 0)
        self.tableView.scrollToRow(at: indexPath, at: .top, animated: true)
    }
    
    //MARK: - Properties
    
    var user: User?

    var stockPickings = [StockPicking]()
    
    var filterType = [Any]()

    var filter = false
    
    private var searchResult = [StockPicking]()
    
    private var sortResult = [StockPicking]()
    
    private var searchActive = false
    
    private var number = 0
    
    private var searchText: String?
    
    private var isDataLoading = false
    
    private var sort = false
    
    private var sortType: SortType?
    
    private var stockpickingType: StockPickingType?
    
    //MARK: - API  functions
    
    //Get  all stockpickings with type by optional filter and order
    private func setupStockPicking(withType type: StockPickingType,filters: [Any]? = nil,at offset: Int = 0, limit: Int = 80,by order: String? = nil) {
        Parameter.domain = ["picking_type_id.code", "=", type.rawValue]
        Parameter.fields = ["name","date","state","hhd_project_id"]
        Parameter.context = ["lang": "en_US",
                             "tz": "Asia/Ho_Chi_Minh"]
        let temp = StockPicking()
        if order != nil && filter {
            print(order!)
            temp.search_read(domain: [Parameter.domain,filterType], fields: Parameter.fields, offset: offset, limit: limit, order: order!, context: Parameter.context, success: { json in
                print(json.count)
                if json != JSON.null && json.count > 0 {
                    for i in 0...json.count - 1 {
                        let stock = temp.getObject(data: json[i])
                        self.sortResult.append(stock)
                        
                    }
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                        self.tableView.separatorStyle = .singleLine
//                        if self.sortResult.count > 0 {
//                            let indexPath = IndexPath(item: 0, section: 0)
//                            self.tableView.scrollToRow(at: indexPath, at: .top, animated: true)
//                        }

                    }
                } else {
                    DispatchQueue.main.async {
                        self.showAlert(withTitle: "Cảnh báo", message: "Không thể load dữ liệu", handler: {
                            self.tableView.reloadData()
                        })
                    }
                }
            }) { error in
                print(error)
            }
        } else if order != nil {
            temp.search_read(domain: [Parameter.domain], fields: Parameter.fields, offset: offset, limit: limit, order: order!, context: Parameter.context, success: { json in
                if json != JSON.null && json.count > 0 {
                    for i in 0...json.count - 1 {
                        let stock = temp.getObject(data: json[i])
                        self.sortResult.append(stock)
                    }
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                        self.tableView.separatorStyle = .singleLine
                        
                    }
                }
            }) { error in
                print(error)
            }
        }
        else if filters != nil {
            temp.search_read(domain: [Parameter.domain,filterType], fields: Parameter.fields, offset: offset, limit: limit, order: "", context: Parameter.context, success: { json in
                if json != JSON.null && json.count > 0 {
                    for i in 0...json.count - 1 {
                        let stock = temp.getObject(data: json[i])
                        self.stockPickings.append(stock)
                    }
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                        self.tableView.separatorStyle = .singleLine
                    }
                } else {
                    self.stockPickings.removeAll()
                    DispatchQueue.main.async {
                        self.showAlert(withTitle: "Cảnh báo", message: "Không thể load dữ liệu", handler: {
                            self.tableView.reloadData()
                        })
                    }
                }
            }) { error in
                print(error)
                
            }
        }
        else {
            temp.search_read(domain: [Parameter.domain], fields: Parameter.fields, offset: offset, limit: limit, order: "", context: Parameter.context, success: { json in
                if json != JSON.null && json.count > 0 {
                    for i in 0...json.count - 1 {
                        let stock = temp.getObject(data: json[i])
                        self.stockPickings.append(stock)
                    }
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                        self.tableView.setContentOffset(.zero, animated: true)
                        self.tableView.separatorStyle = .singleLine
                    }
                }
            }) { error in
                print(error)
            }
        }
        
    }
    
    //Change selected row color
    private func change(color: UIColor,ofSelectedRow cell: UITableViewCell) {
        let bgColorView = UIView()
        bgColorView.backgroundColor = color
        cell.selectedBackgroundView = bgColorView
    }
    
    //Search Stockpicking 's name by optional filter and sort
    private func searchProject(by type: StockPickingType,filter: [Any]? = nil,with text: String,at offset: Int = 0, limit: Int = 80,by order: String? = nil) {
        let temp = StockPicking()
        Parameter.domain = ["hhd_project_id", "ilike", text]
        Parameter.fields = ["name","date","state","hhd_project_id"]
        Parameter.context = ["lang": "en_US",
                             "tz": "Asia/Ho_Chi_Minh"]
        if order != nil && self.filter {
            
            temp.search_read(domain: [Parameter.domain,["picking_type_id.code","=",type.rawValue],filterType], fields: Parameter.fields, offset: offset, limit: limit, order: order!, context: Parameter.context, success: { json in
                if json != JSON.null && json.count > 0 {
                    for i in 0...json.count - 1 {
                        let stock = temp.getObject(data: json[i])
                        self.searchResult.append(stock)
                    }
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                    }
                }else {
                    DispatchQueue.main.async {
                        self.searchResult.removeAll()
                        self.tableView.separatorStyle = .none
                        self.tableView.reloadData()
                    }
                }
            }) { error in
                print(error)
            }
        }else if order != nil {
            temp.search_read(domain: [Parameter.domain], fields: Parameter.fields, offset: offset, limit: limit, order: order!, context: Parameter.context, success: { json in
                if json != JSON.null && json.count > 0 {
                    for i in 0...json.count - 1 {
                        let stock = temp.getObject(data: json[i])
                        self.sortResult.append(stock)
                    }
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                    }
                }
            }) { error in
                print(error)
            }
        }
        else if filter != nil {
            temp.search_read(domain: [Parameter.domain,["picking_type_id.code","=",type.rawValue],filter!], fields: Parameter.fields, offset: offset, limit: limit, order: "", context: Parameter.context, success: { json in
                if json != JSON.null && json.count > 0 {
                    for i in 0...json.count - 1 {
                        let stock = temp.getObject(data: json[i])
                        self.searchResult.append(stock)
                    }
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                    }
                }else {
                    DispatchQueue.main.async {
                        self.searchResult.removeAll()
                        self.tableView.separatorStyle = .none
                        self.tableView.reloadData()
                    }
                }
            }) { error in
                print(error)
            }
        }  else {
            temp.search_read(domain: [Parameter.domain,["picking_type_id.code","=",type.rawValue]], fields: Parameter.fields, offset: offset, limit: limit, order: "", context: Parameter.context, success: { json in
                if json != JSON.null && json.count > 0 {
                    for i in 0...json.count - 1 {
                        let stock = temp.getObject(data: json[i])
                        self.searchResult.append(stock)
                    }
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                    }
                }else {
                    DispatchQueue.main.async {
                        self.searchResult.removeAll()
                        self.tableView.separatorStyle = .none
                        self.tableView.reloadData()
                    }
                }
            }) { error in
                print(error)
            }
        }
        
    }
    
    // MARK: - Controller LifeCycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        stockpickingType = .outgoing
        setupStockPicking(withType: .outgoing)
        
    }
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searchActive {
            if self.searchResult.count == 0 {
                let noDataLabel: UILabel     = UILabel(frame: CGRect(x: 0, y: 0, width: self.tableView.bounds.size.width, height: tableView.bounds.size.height))
                noDataLabel.center = tableView.center
                noDataLabel.text          = "Không có kết quả."
                noDataLabel.textColor     = UIColor.red
                noDataLabel.textAlignment = .center
                self.tableView.backgroundView  = noDataLabel
                return 0
            }
            return searchResult.count
        } else if sort {
            return sortResult.count
        }
        if stockPickings.count == 0 {
            tableView.separatorStyle = .none
            return 0
        }
        return stockPickings.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "StockPickingCell", for: indexPath) as! StockPickingCell
        
        change(color: UIColor(r: 251, g: 153, b: 140), ofSelectedRow: cell)
        if searchActive {
            cell.stockPicking = searchResult[indexPath.row]
        } else if sort {
            if sortResult.count > 0 {
                cell.stockPicking = sortResult[indexPath.row]
            }
        } else {
            cell.stockPicking = stockPickings[indexPath.row]
        }
        
        

        return cell
    }
    
    //MARK: Load more stockpicking
    override func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        print(number)
        if ((tableView.contentOffset.y + tableView.frame.size.height) >= tableView.contentSize.height) {
            isDataLoading = !isDataLoading
            if !isDataLoading {
                number += 5
                let offset = (number - 1) * 20
                if searchActive {
                    if sort {
                        let order = "name " + sortType!.rawValue
                        self.searchProject(by: stockpickingType!, with: searchText!, at: offset, by: order)
                    } else {
                        self.searchProject(by: stockpickingType!, with: searchText!, at: offset)
                    }
                } else if sort {
                    let order = "name " + sortType!.rawValue
                    self.setupStockPicking(withType: stockpickingType!, at: offset, limit: 80, by: order)
                }
                else {
                    self.setupStockPicking(withType: stockpickingType!, at: offset)
                }
                isDataLoading = !isDataLoading
            }
        }
    }
    
    //Display spinner when load more stockpicking
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastSectionIndex = tableView.numberOfSections - 1
        let lastRowIndex = tableView.numberOfRows(inSection: lastSectionIndex) - 1
        if indexPath.section ==  lastSectionIndex && indexPath.row == lastRowIndex {
            if searchResult.count > 0 || stockPickings.count > 0 || sortResult.count > 0 {
                let spinner = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
                spinner.color = .black
                spinner.startAnimating()
                spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(44))
                
                tableView.tableFooterView = spinner
                tableView.tableFooterView?.isHidden = false
            }
        }
        else {
            tableView.tableFooterView?.isHidden = true
        }
    }
}


