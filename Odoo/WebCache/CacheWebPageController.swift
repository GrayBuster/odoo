//
//  CacheWebPageController.swift
//  Odoo
//
//  Created by gray buster on 9/21/18.
//  Copyright © 2018 gray buster. All rights reserved.
//

import UIKit
import WebKit
import Alamofire
import CoreData

enum UrlPrefix: String {
    case http = "http://"
    case https = "https://"
    var description: String {
        return rawValue
    }
}

class CacheWebPageController: UIViewController,WKNavigationDelegate,WKUIDelegate,UITextFieldDelegate,UIWebViewDelegate {

    var urls = ["google":"https://www.google.com/"]
    
    var isCount = false
    var myTimer: Timer?
    
    @IBOutlet weak var containerWebView: UIView!
    
    @IBOutlet weak var progressView: UIProgressView!
    
    @IBOutlet weak var barView: UIView!
    
    @IBOutlet weak var urlTextField: UITextField!
    
    @IBOutlet weak var refreshCacheBtn: UIButton! {
        didSet {
            refreshCacheBtn.addTarget(self, action: #selector(deleteCacheInCoreData), for: .touchUpInside)
        }
    }
    
    @IBOutlet weak var webView: WKWebView!
    
    @IBOutlet weak var anotherWebView: UIWebView!
    
    
    @objc func deleteCacheInCoreData() {
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext

        let fetchRequest = NSFetchRequest<UrlModel>(entityName: "CachedURLResponse")
        fetchRequest.entity = NSEntityDescription.entity(forEntityName: "CachedURLResponse", in: context)
        fetchRequest.includesPropertyValues = false
        do {
            let results = try context.fetch(fetchRequest)
            for result in results {
                context.delete(result)
            }
            try context.save()
        } catch {
            print("fetch error -\(error.localizedDescription)")
        }
    }
    
    func setupWebView() {
        //        webView.navigationDelegate = self
        //        webView.uiDelegate = self
        if let url = URL(string: UrlPrefix.https.description+"facebook.com") {
            let myRequest = URLRequest(url: url)
            //webView.load(myRequest)
            
            
            
            anotherWebView.loadRequest(myRequest)
            
            //webView.allowsBackForwardNavigationGestures = true
            //anotherWebView.addObserver(self, forKeyPath: "estimatedProgress", options: .new, context: nil)
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        urlTextField.resignFirstResponder()
        if textField == urlTextField {
            if textField.text != "" {
                let text = textField.text!
                var finalUrlString = ""
                if !text.contains(UrlPrefix.https.rawValue) {
                   finalUrlString = UrlPrefix.https.description + text
                } else if !text.contains(UrlPrefix.http.rawValue) {
                    finalUrlString = UrlPrefix.https.description + text
                } else {
                    finalUrlString = text
                }
                if let url = URL(string: finalUrlString) {
//                    cacheFile(for: finalUrlString)
                    let myRequest = URLRequest(url: url)
                    anotherWebView.loadRequest(myRequest)
                    //webView.load(myRequest)
                    textField.text = nil
                    return true
                }
            }
        }
        return false
    }
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        self.progressView.progress = 0.0
        self.progressView.isHidden = false
        self.isCount = false
        self.myTimer = Timer.scheduledTimer(timeInterval: 0.01667, target: self, selector: #selector(timerCallback), userInfo: nil, repeats: true)
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        self.isCount = true
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        print(error)
    }
    
    @objc func timerCallback() {
        if self.isCount {
            if self.progressView.progress >= 1 {
                UIView.animate(withDuration: 0.1) {
                    self.progressView.isHidden = true
                }
                self.myTimer?.invalidate()
            } else {
                self.progressView.progress += 0.1
            }
        } else {
            self.progressView.progress += 0.05
            if self.progressView.progress >= 0.95 {
                self.progressView.progress = 0.95
            }
        }
    }

    
    func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
        print(error.localizedDescription)
        let alert = UIAlertController(title: "Error", message: "Url is inappropriate!", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        present(alert, animated: true, completion: nil)
    }
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
    
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        progressView.setProgress(0.0, animated: false)
    }
    
    
    func cacheFile(for url: String? = nil,name: String? = nil) {
        //Create the file/directory pointer for the storage of the cache.
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        guard let dataPath = paths.first?.appending("cache.html") else {
            return
        }
        
        //Check to see if a file exists a the location
        if FileManager.default.fileExists(atPath: dataPath) {
            //Code for customising when the cache reloads would go here.
        } else if url != nil {
            if let cacheUrl = URL(string: url!) {
                //If no file exists write the html cache to it
                //Download and write to file
                do {
                    let cacheUrlData = try Data(contentsOf: cacheUrl)
                    try cacheUrlData.write(to: URL(fileURLWithPath: dataPath), options: Data.WritingOptions.atomic)
                } catch {
                    print("Problem with cacheUrlData")
                }
            }
        }
        
        //Run the load web view function.
        if url != nil {
            if !NetworkState.isConnected() {
                loadWebView(dataPath: dataPath)
            } else {
                loadWebView(dataPath: dataPath,url: url)
            }
        }
    }
    
    func loadWebView(dataPath: String,url: String? = nil) {
        if url != nil {
            webView.load(URLRequest(url: URL(string: url!)!))
        } else {
            webView.load(URLRequest(url: URL(fileURLWithPath: dataPath)))
        }
        
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "estimatedProgress" {
            progressView.isHidden = webView.estimatedProgress == 1
            progressView.setProgress(Float(webView.estimatedProgress), animated: true)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupWebView()
        //cacheFile(for: urls["google"])
//        webView.allowsBackForwardNavigationGestures = true
//        webView.addObserver(self, forKeyPath: "estimatedProgress", options: .new, context: nil)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    /*
    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
