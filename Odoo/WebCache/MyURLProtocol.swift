//
//  MyURLProtocol.swift
//  Odoo
//
//  Created by gray buster on 9/25/18.
//  Copyright © 2018 gray buster. All rights reserved.
//

import Foundation
import CoreData
import UIKit

var requestCount:Int = 0

class MyURLProtocol: URLProtocol,URLSessionDelegate,URLSessionTaskDelegate,URLSessionDataDelegate {
    
    var connection: URLSessionDataTask?
    var mutableData: NSMutableData?
    var response: URLResponse!
    
    class var CustomKey:String {
        return "MyURLProtocolHandledKey"
    }
    
    override class func canInit(with request: URLRequest) -> Bool {
        if URLProtocol.property(forKey: MyURLProtocol.CustomKey, in: request) != nil {
            return false
        }
        return true
    }
    
    
    override class func canonicalRequest(for request: URLRequest) -> URLRequest {
        return request
    }
    
    
    override class func requestIsCacheEquivalent(_: URLRequest, to: URLRequest) -> Bool {
        return true
    }
    
    override func startLoading() {
        self.cachedResponseForCurrentRequest { urlModel,error in
            guard error == nil else { print(error!); return }
            if urlModel != nil {
                if let cachedResponse = urlModel {
                    print("Serving response from cache")
                    let data = cachedResponse.data!.data(using: .utf8)
                    if cachedResponse.encoding != nil {
                        
                        let response = URLResponse(url: self.request.url!, mimeType: cachedResponse.mimeType, expectedContentLength: data!.count, textEncodingName: cachedResponse.encoding)
                        self.client!.urlProtocol(self, didReceive: response, cacheStoragePolicy: .notAllowed)
                        self.client!.urlProtocol(self, didLoad: data!)
                        //self.client?.urlProtocolDidFinishLoading(self)
                    } else {
                        let response = URLResponse(url: self.request.url!, mimeType: cachedResponse.mimeType, expectedContentLength: data!.count, textEncodingName: nil)
                        self.client!.urlProtocol(self, didReceive: response, cacheStoragePolicy: .notAllowed)
                        self.client!.urlProtocol(self, didLoad: data!)
                        
                    }
                    self.client?.urlProtocolDidFinishLoading(self)
                    //print(cachedResponse.url!)
                    
                    // 4
                    
                }
            }else {
                print("Serving new response")
                let newRequest = self.request as! NSMutableURLRequest
                URLProtocol.setProperty("true", forKey: MyURLProtocol.CustomKey, in: newRequest)
                
                //self.connection = URLSession.shared.dataTask(with: newRequest as URLRequest)
                let defaultConfigObj = URLSessionConfiguration.default
                let defaultSession = URLSession(configuration: defaultConfigObj, delegate: self, delegateQueue: nil)
                
                
                self.connection = defaultSession.dataTask(with: newRequest as URLRequest)
                self.connection!.resume()
            }
        }
        
        
    }
    
    override func stopLoading() {
        if self.connection != nil {
            self.connection!.cancel()
            //self.connection = nil
//            self.mutableData = nil
//            self.response = nil
        }
        
        
    }
    // MARK: URLSessionDataDelegate

    func urlSession(_ session: URLSession, dataTask: URLSessionDataTask, didReceive response: URLResponse, completionHandler: @escaping (URLSession.ResponseDisposition) -> Void) {
        self.client!.urlProtocol(self, didReceive: response, cacheStoragePolicy: .notAllowed)
        self.response = response
        self.mutableData = NSMutableData()
        completionHandler(.allow)
    }
    
    func urlSession(_ session: URLSession, dataTask: URLSessionDataTask, didReceive data: Data) {
        self.client?.urlProtocol(self, didLoad: data)
        
        self.mutableData!.append(data)
    }
    // MARK: URLSessionTaskDelegate
    
    func urlSession(_ session: URLSession, task: URLSessionTask, didCompleteWithError error: Error?) {
        if error != nil  {
            self.client?.urlProtocol(self, didFailWithError: error!)
            print(error!)
        } else {
            saveCachedResponse()
            self.client?.urlProtocolDidFinishLoading(self)
        }
    }
    
//    func connection(connection: URLSessionDataTask!, didReceiveResponse response: URLResponse!) {
//        self.client!.urlProtocol(self, didReceive: response, cacheStoragePolicy: .notAllowed)
//        self.response = response
//        self.mutableData = NSMutableData()
//    }
//
//
//    func connection(connection: URLSessionDataTask!, didReceiveData data: Data!) {
//        self.client!.urlProtocol(self, didLoad: data)
//        self.mutableData.append(data)
//    }
//
//    func connectionDidFinishLoading(connection: URLSessionDataTask!) {
//        self.client!.urlProtocolDidFinishLoading(self)
//        self.saveCachedResponse()
//    }
//
//    func connection(connection: URLSessionDataTask!, didFailWithError error: Error!) {
//        self.client!.urlProtocol(self, didFailWithError: error)
//    }
    
    func saveCachedResponse () {
        
        DispatchQueue.main.async {
            print("Saving cached response")
            
            // 1
            let delegate = UIApplication.shared.delegate as! AppDelegate
            let context = delegate.persistentContainer.viewContext
            DispatchQueue.global().async {
                //2
                let cachedResponse = NSEntityDescription.insertNewObject(forEntityName: "CachedURLResponse", into: context) as NSManagedObject
                if let dataString = (self.mutableData! as Data).toString(){
                    cachedResponse.setValue(dataString, forKey: "data")
                    cachedResponse.setValue(self.request.url?.absoluteString, forKey: "url")
                    cachedResponse.setValue(Date(), forKey: "timestamp")
                    cachedResponse.setValue(self.response.mimeType!, forKey: "mimeType")
                    cachedResponse.setValue(self.response.textEncodingName, forKey: "encoding")
                    
                } else { print("could not convert") }
            }
            
            do {
                if context.hasChanges {
                    try context.save()
                }
            } catch let error {
                print(error)
            }
            
        }

    }

    func cachedResponseForCurrentRequest(handler: @escaping((UrlModel?,Error?) -> ())) {
        DispatchQueue.main.async {
            // 1
            let delegate = UIApplication.shared.delegate as! AppDelegate
            let context = delegate.persistentContainer.viewContext
            
            // 2
            let fetchRequest = NSFetchRequest<UrlModel>(entityName: "CachedURLResponse")
            let entity = NSEntityDescription.entity(forEntityName: "CachedURLResponse", in: context)
            fetchRequest.entity = entity
            
            // 3
            let predicate = NSPredicate(format:"url == %@", self.request.url!.absoluteString)
            fetchRequest.predicate = predicate
            
            // 4
            do {
                let possibleResult = try context.fetch(fetchRequest)
                // 5
                if let firstResult = possibleResult.first {
                    handler(firstResult,nil)
                } else {
                    handler(nil,nil)
                }
            } catch let error {
                handler(nil,error)
            }
            
        }
    }
    
}

class AnotherUrlModel: NSObject {
    var data: Data?
    var urlString: String?
    var timeStamp: Date?
    
    static func getObject(data: Data,url:String,time: Date) -> AnotherUrlModel {
        let model = AnotherUrlModel()
        model.data = data
        model.timeStamp = time
        model.urlString = url
        return model
    }
}
