//
//  CheckBox.swift
//  Odoo
//
//  Created by gray buster on 9/12/18.
//  Copyright © 2018 gray buster. All rights reserved.
//

import UIKit

@IBDesignable
class CheckBox: UIButton {
    
    // Images
    @IBInspectable
    var checkedImage: UIImage = UIImage() 
    
    @IBInspectable
    var uncheckedImage:UIImage = UIImage()
    
    // Bool property
    var isChecked: Bool = false {
        didSet{
            if isChecked == true {
                self.setImage(checkedImage, for: UIControlState.normal)
            } else {
                self.setImage(uncheckedImage, for: UIControlState.normal)
            }
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        self.addTarget(self, action:#selector(buttonClicked(sender:)), for: UIControlEvents.touchUpInside)
        self.isChecked = false
    }
    
//    override init(frame: CGRect) {
//        super.init(frame: frame)
//        
//    }
//    
//    required init?(coder aDecoder: NSCoder) {
//        fatalError("init(coder:) has not been implemented")
//    }
    
    @objc func buttonClicked(sender: UIButton) {
        if sender == self {
            isChecked = !isChecked
        }
    }

}
