//
//  Extension.swift
//  Odoo
//
//  Created by gray buster on 9/12/18.
//  Copyright © 2018 gray buster. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import SystemConfiguration

// MARK: Underline text
extension String {
    func getUnderLineAttributedText() -> NSAttributedString {
        return NSMutableAttributedString(string: self, attributes: [.underlineStyle: NSUnderlineStyle.styleSingle.rawValue])
    }
}

// MARK:Add bottom border
extension UIView {
    func addBottomBorderWithColor(_ color: UIColor, width: CGFloat) {
        let border = CALayer()
        border.backgroundColor = color.cgColor
        border.frame = CGRect(x:0, y:self.frame.size.height - width, width:self.frame.size.width, height:width)
        self.layer.addSublayer(border)
    }
    func addTopBorderToView(_ color: UIColor,lineWidth: CGFloat) {
        let topBorder = CAShapeLayer()
        let topPath = UIBezierPath()
        topPath.move(to: CGPoint(x: 0, y: 0))
        topPath.addLine(to: CGPoint(x: self.frame.width, y: 0))
        topBorder.path = topPath.cgPath
        topBorder.strokeColor = color.cgColor
        topBorder.lineWidth = lineWidth
        topBorder.fillColor = color.cgColor
        self.layer.addSublayer(topBorder)
    }
    
    
}

//MARK: Extension UITableView
extension UITableView {
    func addBottomBorderToTableView(_ color: UIColor,lineWidth: CGFloat) {
        let bottomBorder = CAShapeLayer()
        let bottomPath = UIBezierPath()
        bottomPath.move(to: CGPoint(x: 0, y: self.frame.height))
        bottomPath.addLine(to: CGPoint(x: self.frame.width, y: self.frame.height))
        bottomBorder.path = bottomPath.cgPath
        bottomBorder.strokeColor = color.cgColor
        bottomBorder.lineWidth = lineWidth
        bottomBorder.fillColor = color.cgColor
        self.layer.addSublayer(bottomBorder)
    }
}

// MARK: Init color with RGB
extension UIColor {
    convenience init(r: CGFloat,g: CGFloat,b: CGFloat) {
        self.init(red: r/255, green: g/255, blue: b/255, alpha: 1)
    }
}

// MARK: Tab Bar Extension
extension UITabBar {
    
    //Set font ,size and color of custom font of tab bar item title
    func change(fontSize size: CGFloat, font: String,underlineColor: UIColor?,fontColor: UIColor?,fontSelectedColor: UIColor?,lineWidth: CGFloat?) {
        if underlineColor != nil && fontColor != nil && fontSelectedColor != nil && lineWidth != nil {
            self.selectionIndicatorImage = UIImage().createSelectionIndicator(color: underlineColor!, size: CGSize(width: self.bounds.width/CGFloat(self.items!.count), height: self.bounds.height), lineWidth: lineWidth!)
            for item in self.items! {
                item.setTitleTextAttributes([NSAttributedStringKey.font: UIFont(name: font, size: size)!, NSAttributedStringKey.foregroundColor: fontColor!], for: .normal)
                item.setTitleTextAttributes([NSAttributedStringKey.font: UIFont(name: font, size:size)!, NSAttributedStringKey.foregroundColor: fontSelectedColor!], for: .selected)
            }
        } else if fontColor != nil && fontSelectedColor != nil {
            for item in self.items! {
                item.setTitleTextAttributes([NSAttributedStringKey.font: UIFont(name: font, size: size)!, NSAttributedStringKey.foregroundColor: fontColor!], for: .normal)
                item.setTitleTextAttributes([NSAttributedStringKey.font: UIFont(name: font, size:size)!, NSAttributedStringKey.foregroundColor: fontSelectedColor!], for: .selected)
            }
        }
    }
    
    //Change font size of system font
    func change(size: CGFloat,weight: UIFont.Weight
        ,underlineColor: UIColor?,fontColor: UIColor?,fontSelectedColor: UIColor?,lineWidth: CGFloat?) {
        if underlineColor != nil && fontColor != nil && fontSelectedColor != nil && lineWidth != nil {
            self.selectionIndicatorImage = UIImage().createSelectionIndicator(color: underlineColor!, size: CGSize(width: self.bounds.width/CGFloat(self.items!.count), height: self.bounds.height), lineWidth: lineWidth!)
            for item in self.items! {
                item.setTitleTextAttributes([NSAttributedStringKey.font: UIFont.systemFont(ofSize: size, weight: weight), NSAttributedStringKey.foregroundColor: fontColor!], for: .normal)
                item.setTitleTextAttributes([NSAttributedStringKey.font: UIFont.systemFont(ofSize: size, weight: weight), NSAttributedStringKey.foregroundColor: fontSelectedColor!], for: .selected)
            }
        } else if fontColor != nil && fontSelectedColor != nil {
            for item in self.items! {
                item.setTitleTextAttributes([NSAttributedStringKey.font: UIFont.systemFont(ofSize: size, weight: weight), NSAttributedStringKey.foregroundColor: fontColor!], for: .normal)
                item.setTitleTextAttributes([NSAttributedStringKey.font: UIFont.systemFont(ofSize: size, weight: weight), NSAttributedStringKey.foregroundColor: fontSelectedColor!], for: .selected)
            }
        }
    }
    
    //Add Tab bar seperator
    func setupTabBarSeparators(withSeparatorWidth width: CGFloat,_ color: UIColor) {
        let itemWidth = floor(self.frame.size.width / CGFloat(self.items!.count))

        // iterate through the items in the Tab Bar, except the last one
        for i in 0...(self.items!.count - 1) {
            // make a new separator at the end of each tab bar item
            let separator = UIView(frame: CGRect(x: itemWidth * CGFloat(i + 1) - CGFloat(width / 2), y: 0, width: CGFloat(width), height: self.frame.size.height))
            
            // set the color to seperator (default line color for tab bar)
            separator.backgroundColor = color
            
            self.addSubview(separator)
        }
    }
}


// MARK: Image Extension
extension UIImage {
    
    // Highlight selected tab
    func createSelectionIndicator(color: UIColor, size: CGSize, lineWidth: CGFloat) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        color.setFill()
        UIRectFill(CGRect(x: 0,y: size.height - lineWidth, width: size.width,height: lineWidth))
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image!
    }
    
    class func imageWithColor(color: UIColor, size: CGSize) -> UIImage {
        let rect: CGRect = CGRect(x: 0, y: 0, width: size.width, height: size.height)
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        color.setFill()
        UIRectFill(rect)
        let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return image
    }
    
    class func getColoredRectImageWith(color: CGColor, andSize size: CGSize) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(size, false, 0.0)
        let graphicsContext = UIGraphicsGetCurrentContext()
        graphicsContext?.setFillColor(color)
        let rectangle = CGRect(x: 0.0, y: 0.0, width: size.width, height: size.height)
        graphicsContext?.fill(rectangle)
        let rectangleImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return rectangleImage!
    }
}

// MARK: UIViewController Extension
extension UIViewController {
    var content: UIViewController {
        if let navcon = self as? UINavigationController {
            return navcon.visibleViewController ?? self
        }
        return self
    }
}


// MARK: Extension of UIButton
extension UIButton {
    func change(fontName: String,withFontSize size: CGFloat,for state: UIControlState) {
        self.setAttributedTitle(NSAttributedString(string: "", attributes: [NSAttributedStringKey.font: UIFont(name: fontName, size: size)!, NSAttributedStringKey.foregroundColor: UIColor.white]), for: state)
    }
}

// MARK: Extension of UIBarButtonItem
extension UIBarButtonItem {
    func change(fontName: String, withFontSize size: CGFloat,for state: UIControlState) {
        self.setTitleTextAttributes([NSAttributedStringKey.font: UIFont(name: fontName, size: size)!, NSAttributedStringKey.foregroundColor: UIColor.white], for: state)
    }
}

//MARK : Extension of UISearchBar
extension UISearchBar {
    //Change position of search image 's icon
    func changePositionToRightOfSearchImageIcon(withWidth width: CGFloat , height: CGFloat,color: UIColor) {
        let searchTextField:UITextField = self.subviews[0].subviews.last as! UITextField
        searchTextField.textAlignment = NSTextAlignment.left
        let image:UIImage = UIImage.fontAwesomeIcon(name: .search, style: .solid, textColor: color, size: CGSize(width: width , height: height))
        let imageView:UIImageView = UIImageView.init(image: image)
        searchTextField.leftView = nil
        searchTextField.rightView = imageView
        searchTextField.rightViewMode = UITextFieldViewMode.always
    }
}

//MARK : Extension of UIViewController
extension UIViewController {
    func showAlert(withTitle title:String, message: String,handler: (() -> ())?) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        if handler != nil {
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { _ in
                handler!()
            }))
        }else {
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        }
        self.present(alert, animated: true, completion: nil)
    }
   
}

//MARKL - Checking network class
class NetworkState {
    class func isConnected() ->Bool {
        return NetworkReachabilityManager()!.isReachable
    }
}



class Reachability {
    var hostname: String?
    var isRunning = false
    var isReachableOnWWAN: Bool
    var reachability: SCNetworkReachability?
    var reachabilityFlags = SCNetworkReachabilityFlags()
    let reachabilitySerialQueue = DispatchQueue(label: "ReachabilityQueue")
    init?(hostname: String) throws {
        guard let reachability = SCNetworkReachabilityCreateWithName(nil, hostname) else {
            throw Network.Error.failedToCreateWith(hostname)
        }
        self.reachability = reachability
        self.hostname = hostname
        isReachableOnWWAN = true
    }
    init?() throws {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout<sockaddr_in>.size)
        zeroAddress.sin_family = sa_family_t(AF_INET)
        guard let reachability = withUnsafePointer(to: &zeroAddress, {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {
                SCNetworkReachabilityCreateWithAddress(nil, $0)
            }}) else {
                throw Network.Error.failedToInitializeWith(zeroAddress)
        }
        self.reachability = reachability
        isReachableOnWWAN = true
    }
    var status: Network.Status {
        return  !isConnectedToNetwork ? .unreachable :
            isReachableViaWiFi    ? .wifi :
            isRunningOnDevice     ? .wwan : .unreachable
    }
    var isRunningOnDevice: Bool = {
        #if targetEnvironment(simulator)
        return false
        #else
        return true
        #endif
    }()
    deinit { stop() }
}

extension Reachability {
    func start() throws {
        guard let reachability = reachability, !isRunning else { return }
        var context = SCNetworkReachabilityContext(version: 0, info: nil, retain: nil, release: nil, copyDescription: nil)
        context.info = Unmanaged<Reachability>.passUnretained(self).toOpaque()
        guard SCNetworkReachabilitySetCallback(reachability, callout, &context) else { stop()
            throw Network.Error.failedToSetCallout
        }
        guard SCNetworkReachabilitySetDispatchQueue(reachability, reachabilitySerialQueue) else { stop()
            throw Network.Error.failedToSetDispatchQueue
        }
        reachabilitySerialQueue.async { self.flagsChanged() }
        isRunning = true
    }
    func stop() {
        defer { isRunning = false }
        guard let reachability = reachability else { return }
        SCNetworkReachabilitySetCallback(reachability, nil, nil)
        SCNetworkReachabilitySetDispatchQueue(reachability, nil)
        self.reachability = nil
    }
    var isConnectedToNetwork: Bool {
        return isReachable &&
            !isConnectionRequiredAndTransientConnection &&
            !(isRunningOnDevice && isWWAN && !isReachableOnWWAN)
    }
    var isReachableViaWiFi: Bool {
        return isReachable && isRunningOnDevice && !isWWAN
    }
    
    /// Flags that indicate the reachability of a network node name or address, including whether a connection is required, and whether some user intervention might be required when establishing a connection.
    var flags: SCNetworkReachabilityFlags? {
        guard let reachability = reachability else { return nil }
        var flags = SCNetworkReachabilityFlags()
        return withUnsafeMutablePointer(to: &flags) {
            SCNetworkReachabilityGetFlags(reachability, UnsafeMutablePointer($0))
            } ? flags : nil
    }
    
    /// compares the current flags with the previous flags and if changed posts a flagsChanged notification
    func flagsChanged() {
        guard let flags = flags, flags != reachabilityFlags else { return }
        reachabilityFlags = flags
        NotificationCenter.default.post(name: .flagsChanged, object: self)
    }
    
    /// The specified node name or address can be reached via a transient connection, such as PPP.
    var transientConnection: Bool { return flags?.contains(.transientConnection) == true }
    
    /// The specified node name or address can be reached using the current network configuration.
    var isReachable: Bool { return flags?.contains(.reachable) == true }
    
    /// The specified node name or address can be reached using the current network configuration, but a connection must first be established. If this flag is set, the kSCNetworkReachabilityFlagsConnectionOnTraffic flag, kSCNetworkReachabilityFlagsConnectionOnDemand flag, or kSCNetworkReachabilityFlagsIsWWAN flag is also typically set to indicate the type of connection required. If the user must manually make the connection, the kSCNetworkReachabilityFlagsInterventionRequired flag is also set.
    var connectionRequired: Bool { return flags?.contains(.connectionRequired) == true }
    
    /// The specified node name or address can be reached using the current network configuration, but a connection must first be established. Any traffic directed to the specified name or address will initiate the connection.
    var connectionOnTraffic: Bool { return flags?.contains(.connectionOnTraffic) == true }
    
    /// The specified node name or address can be reached using the current network configuration, but a connection must first be established.
    var interventionRequired: Bool { return flags?.contains(.interventionRequired) == true }
    
    /// The specified node name or address can be reached using the current network configuration, but a connection must first be established. The connection will be established "On Demand" by the CFSocketStream programming interface (see CFStream Socket Additions for information on this). Other functions will not establish the connection.
    var connectionOnDemand: Bool { return flags?.contains(.connectionOnDemand) == true }
    
    /// The specified node name or address is one that is associated with a network interface on the current system.
    var isLocalAddress: Bool { return flags?.contains(.isLocalAddress) == true }
    
    /// Network traffic to the specified node name or address will not go through a gateway, but is routed directly to one of the interfaces in the system.
    var isDirect: Bool { return flags?.contains(.isDirect) == true }
    
    /// The specified node name or address can be reached via a cellular connection, such as EDGE or GPRS.
    var isWWAN: Bool { return flags?.contains(.isWWAN) == true }
    
    /// The specified node name or address can be reached using the current network configuration, but a connection must first be established. If this flag is set
    /// The specified node name or address can be reached via a transient connection, such as PPP.
    var isConnectionRequiredAndTransientConnection: Bool {
        return (flags?.intersection([.connectionRequired, .transientConnection]) == [.connectionRequired, .transientConnection]) == true
    }
}

func callout(reachability: SCNetworkReachability, flags: SCNetworkReachabilityFlags, info: UnsafeMutableRawPointer?) {
    guard let info = info else { return }
    DispatchQueue.main.async {
        Unmanaged<Reachability>.fromOpaque(info).takeUnretainedValue().flagsChanged()
    }
}

extension Notification.Name {
    static let flagsChanged = Notification.Name("FlagsChanged")
}

struct Network {
    static var reachability: Reachability?
    enum Status: String, CustomStringConvertible {
        case unreachable, wifi, wwan
        var description: String { return rawValue }
    }
    enum Error: Swift.Error {
        case failedToSetCallout
        case failedToSetDispatchQueue
        case failedToCreateWith(String)
        case failedToInitializeWith(sockaddr_in)
    }
}

extension Data {
    func toString() -> String?  {
        return String(data: self, encoding: .utf8)
    }
}


