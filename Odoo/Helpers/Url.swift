//
//  Url.swift
//  Odoo
//
//  Created by gray buster on 9/12/18.
//  Copyright © 2018 gray buster. All rights reserved.
//

import Foundation
import SwiftyJSON
import Alamofire

struct Url {
    static let url = "https://test01.e300.vn/"
}

struct Function {
    static let Login = "web/session/authenticate"
}

struct Parameter {
    static var params: Parameters = [:]
    static var kwargs: [String: Any] = [:]
    static var domain = [Any]()
    static var fields = [String]()
    static var context = [String:Any]()
}
