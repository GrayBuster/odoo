
//  File.swift
//  warehouse_api
//
//  Created by Broshop on 10/4/17.
//  Copyright © 2017 Mac01. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import UIKit


public class API {
    //func search read
    func callJsonRpc(args: Parameters,function: String = "web/dataset/call_kw", success:@escaping((JSON) -> Void), failure:@escaping((Error) -> Void)){
        let params: Parameters = [
            "params" : args
        ]
        Alamofire.request(Url.url + function , method: .post, parameters: params, encoding: JSONEncoding.default).validate().responseJSON
            {
                response in
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
                    if json["result"] != JSON.null{
                        success(json["result"])
                    }else
                     if json["error"] != JSON.null {
                        if let result = json["error"].dictionary{
                            if let data = result["data"]?.dictionary{
                                success(data["message"]!)
                            }
                        }
                    }
                    break
                case .failure(let error):
                    print(error)
                    failure(error)
                }
        }
    }
}
