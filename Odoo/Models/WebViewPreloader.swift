//
//  WebViewPreloader.swift
//  Odoo
//
//  Created by gray buster on 9/24/18.
//  Copyright © 2018 gray buster. All rights reserved.
//

import Foundation
import WebKit

class WebViewPreloader {
    var webviews = [URL: WKWebView]()
    
    
    /// Registers a web view for preloading. If an webview for that URL already
    /// exists, the web view reloads the request
    ///
    /// - Parameter url: the URL to preload
    func preload(url: URL) {
        webview(for: url).load(URLRequest(url: url))
    }
    
    /// Creates or returns an already cached webview for the given URL.
    /// If the webview doesn't exist, it gets created and asked to load the URL
    ///
    /// - Parameter url: the URL to prefecth
    /// - Returns: a new or existing web view
    func webview(for url: URL) -> WKWebView {
        if let cachedWebView = webviews[url] { return cachedWebView }
        
        let webview = WKWebView(frame: .zero)
        webview.load(URLRequest(url: url))
        webviews[url] = webview
        return webview
    }
}
extension Bundle {
    var indexURL: URL { return self.url(forResource: "index", withExtension: "html")! }
}
