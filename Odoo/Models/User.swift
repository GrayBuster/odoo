//
//  User.swift
//  Odoo
//
//  Created by gray buster on 9/12/18.
//  Copyright © 2018 gray buster. All rights reserved.
//

import Foundation
import SwiftyJSON

class User: Model {

    var username: String
    var currencies: Currencies
    var fcm_project_id: String
    var db: String
    var inbox_action: Int
    var is_superuser: Bool
    var partner_id: Int
    var server_version: String
    var user_context: UserContext
    var name: String
    var server_version_info: [Any]
    var company_id: Int
    var session_id: String
    var device_subscription_ids: [Any]
    var user_companies: UserCompanies
    var image: String
    
    override init() {
        self.username = ""
        self.currencies = Currencies()
        self.image = ""
        self.fcm_project_id = ""
        self.db = ""
        self.inbox_action = 0
        self.is_superuser = false
        self.partner_id = 0
        self.server_version = ""
        self.user_context = UserContext()
        self.name = ""
        self.server_version_info = []
        self.company_id = 0
        self.session_id = ""
        self.device_subscription_ids = []
        self.user_companies = UserCompanies()
    }
    
    
    init(from dict: [String:Any]) {
        
        self.username = dict["username"] as? String ?? ""
        self.currencies = Currencies(dict: dict["currencies"] as! [String:Any])
        self.fcm_project_id = dict["fcm_project_id"] as! String
        self.image = ""
        self.db = dict["db"] as! String
        self.inbox_action = dict["inbox_action"] as! Int
        self.is_superuser = dict["is_admin"] as! Bool
        self.partner_id = dict["partner_id"] as! Int
        self.server_version = dict["server_version"] as! String
        self.user_context = UserContext(dict: dict["user_context"] as! [String:Any])
        self.name = dict["name"] as! String
        self.server_version_info = dict["server_version_info"] as! [Any]
        self.company_id = dict["company_id"] as! Int
        self.session_id = dict["session_id"] as! String
        self.device_subscription_ids = dict["device_subscription_ids"] as! [Any]
        self.user_companies = UserCompanies(dict: dict["user_companies"] as! [String:Any])
        super.init()
        self.id = dict["uid"] as? Int
    }
    
    init(from json: JSON) {
        self.username = json["username"].string ?? ""
        self.currencies = Currencies(from: json["currencies"])
        self.image = ""
        self.fcm_project_id = json["fcm_project_id"].string!
        self.db = json["db"].string!
        self.inbox_action = json["inbox_action"].int!
        self.is_superuser = json["is_superuser"].bool!
        self.partner_id = json["partner_id"].int!
        self.server_version = json["server_version"].string!
        self.user_context = UserContext(from: json["user_context"])
        self.name = json["name"].string!
        self.server_version_info = json["server_version_info"].arrayObject!
        self.company_id = json["company_id"].int!
        self.session_id = json["session_id"].string!
        self.device_subscription_ids = json["device_subscription_ids"].arrayObject!
        self.user_companies = UserCompanies(from: json["user_companies"])
    }
    
    override func getObjectName() -> String {
        return ""
    }
}

struct Currencies {
    var value: (x: Value,y: Value,z: Value)
    init(dict: [String:Any]) {
        value.x = Value(dict: dict["1"] as! [String:Any])
        value.y = Value(dict: dict["3"] as! [String:Any])
        value.z = Value(dict: dict["24"] as! [String:Any])
    }
    
    init(from json: JSON) {
        value.x = Value(from: json["1"])
        value.y = Value(from: json["3"])
        value.z = Value(from: json["24"])
    }
    
    init() {
        value.x = Value()
        value.y = Value()
        value.z = Value()
    }
}



struct Value {
    let digits: [Int]
    let position: String
    let symbol: String
    init(dict: [String:Any]) {
        digits = dict["digits"] as! [Int]
        position = dict["position"] as! String
        symbol = dict["symbol"] as! String
    }
    
    init(from json: JSON) {
        digits = json["digits"].arrayObject as! [Int]
        position = json["position"].string!
        symbol = json["symbol"].string!
    }
    
    init() {
        digits = []
        position = ""
        symbol = ""
    }
}

struct UserContext {
    let lang: String
    let tz: String
    let uid: Int
    
    init(dict: [String:Any]) {
        lang = dict["lang"] as! String
        tz = dict["tz"] as! String
        uid = dict["uid"] as! Int
    }
    
    init(from json: JSON) {
        lang = json["lang"].string!
        tz = json["tz"].string!
        uid = json["uid"].int!
    }
    
    init() {
        lang = ""
        tz = ""
        uid = 0
    }
}

struct UserCompanies {
    let current_company: [Any]
    let allowed_companies: [[Any]]
    
    init(dict: [String:Any]) {
        current_company = dict["current_company"] as! [Any]
        allowed_companies = dict["allowed_companies"] as! [[Any]]
    }
    
    init(from json: JSON) {
        current_company = json["current_company"].arrayObject!
        allowed_companies = [json["allowed_companies"].arrayObject!]
    }
    init() {
        current_company = []
        allowed_companies = [[]]
    }
}


class UserDetail {
    var mobile: String
    var email: String?
    var phone: String?
    var id : Int
    var name: String
    init(from dict: [String:Any]) {
        mobile = dict["mobile"] as! String
        if let email = dict["email"] as? String {
            self.email = email
        }
        self.id = dict["id"] as! Int
        if let phone = dict["phone"] as? String {
            self.phone = phone
        }
        
        self.name = dict["name"] as! String
    }
}

