//
//  Model.swift
//  warehouse_api
//
//  Created by Broshop on 10/10/17.
//  Copyright © 2017 Mac01. All rights reserved.
//

import Foundation
import SwiftyJSON
import Alamofire
import UIKit

public class Model: NSObject{
    var id : Int?
    
    let api = API()
    
    override init() {
    }
    
    func getObjectName()->String{
        return "models.model";
    }
    
    func getImage(data: JSON) -> UIImage{
        print(data.stringValue)
        if(data != JSON.null && data.stringValue != "false" && data.stringValue != ""){
            let decodedData = Data(base64Encoded: data.stringValue, options: .ignoreUnknownCharacters)
            return UIImage(data: decodedData!)!
        }
        else{
             return UIImage(named: "noImage")!
        }
        
    }
    
    func search_read(domain: [Any], fields: [String], offset: Int = 0, limit: Int = 80, order: String, context: Parameters, success:@escaping((JSON) -> Void), failure:@escaping((Error) -> Void)){
       
        let args: Parameters = [
            "model": self.getObjectName(),
            "method" : "search_read",
            "args" : [domain,fields, offset, limit, order],
            "kwargs": Parameter.kwargs,
            "context" : context
        ]
        api.callJsonRpc(args: args, success: success, failure: failure)
    }
    
    func search_count(domain: [[String]], context: Parameters, success:@escaping((JSON) -> Void), failure:@escaping((Error) -> Void)){
        let args: Parameters = [
            "model": self.getObjectName(),
            "method" : "search_count",
            "args" : [domain],
            "kwargs": Parameter.kwargs,
            "context" : context
        ]
        api.callJsonRpc(args: args, success: success, failure: failure)
    }
    
    func read(ids: [Int], fields: [String], context: Parameters, success:@escaping((JSON) -> Void), failure:@escaping((Error) -> Void)){
        let args: Parameters = [
            "model": self.getObjectName(),
            "method" : "read",
            "args" : [ids, fields],
            "kwargs": Parameter.kwargs,
            "context" : context
        ]
        api.callJsonRpc(args: args, success: success, failure: failure)
        
    }
    
    func call_method(method: String, args: [Any], context: Parameters, success:@escaping((JSON) -> Void), failure:@escaping((Error) -> Void)){
        let args: Parameters = [
            "model": self.getObjectName(),
            "method" : method,
            "args" : args,
            "kwargs": Parameter.kwargs,
            "context" : context
        ]
        api.callJsonRpc(args: args, success: success, failure: failure)
    }

    
    func create(vals: Any, context: Parameters, success:@escaping((JSON) -> Void), failure:@escaping((Error) -> Void)){
        let args: Parameters = [
            "model"   : self.getObjectName(),
            "method"  : "create",
            "args"    : vals,
            "kwargs"  : Parameter.kwargs,
            "context" : context
        ]
//        print(args)
        api.callJsonRpc(args: args, success: success, failure: failure)
    }
    
    func write(ids:[Int], vals: Parameters, context: Parameters, success:@escaping((JSON) -> Void), failure:@escaping((Error) -> Void)){
        let args: Parameters = [
            "model"  : self.getObjectName(),
            "method" : "write",
            "args"   : [ids, vals],
            "kwargs" : Parameter.kwargs,
            ]
        api.callJsonRpc(args: args, success: success, failure: failure)
    }
    
    func search(domain: [Any], fields: [String], offset: Int = 0, limit: Int = 80, order: String, context: Parameters, success:@escaping((JSON) -> Void), failure:@escaping((Error) -> Void)) {
        let args: Parameters = [
            "model"   : self.getObjectName(),
            "method"  : "search",
            "args"    : [domain, fields, offset, limit, order],
            "kwargs"  : Parameter.kwargs,
            "context" : context
        ]

        api.callJsonRpc(args: args, success: success, failure: failure)
    }

    
    func unlink(ids:[Int], context: Parameters, success:@escaping((JSON) -> Void), failure:@escaping((Error) -> Void)){
        let args: Parameters = [
            "model": self.getObjectName(),
            "method" : "unlink",
            "args" : [ids],
            "kwargs": Parameter.kwargs,
            "context" : context
        ]
        api.callJsonRpc(args: args, success: success, failure: failure)
    }
}
