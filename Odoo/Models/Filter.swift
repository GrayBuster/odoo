//
//  Filter.swift
//  Odoo
//
//  Created by gray buster on 9/18/18.
//  Copyright © 2018 gray buster. All rights reserved.
//

import Foundation
import SwiftyJSON
import Alamofire

class Filter: Model {
    var name: String?
    var domains: [Any]?
    
    func getObject(withName name: String,domains: [Any]) -> Filter {
        let filter = Filter()
        filter.name = name
        filter.domains = domains
        return filter
    }
    
    override func getObjectName() -> String {
        return "ir.ui.view"
    }
}
