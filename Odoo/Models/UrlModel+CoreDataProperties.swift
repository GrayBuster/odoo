//
//  UrlModel+CoreDataProperties.swift
//  Odoo
//
//  Created by gray buster on 9/25/18.
//  Copyright © 2018 gray buster. All rights reserved.
//
//

import Foundation
import CoreData


extension UrlModel {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<UrlModel> {
        return NSFetchRequest<UrlModel>(entityName: "CachedURLResponse")
    }

    @NSManaged public var data: String?
    @NSManaged public var encoding: String?
    @NSManaged public var mimeType: String?
    @NSManaged public var timestamp: NSDate?
    @NSManaged public var url: String?

}
