//
//  StockPicking.swift
//  Odoo
//
//  Created by gray buster on 9/13/18.
//  Copyright © 2018 gray buster. All rights reserved.
//

import Foundation
import SwiftyJSON
import Alamofire

class StockPicking: Model {
    
    var state: String?
    var name: String?
    var date: String?
    var hhdProject: [Any]?
    
//    init(from json: JSON) {
//        state = json["state"].string!
//        name = json["name"].string!
//        date = json["date"].string!
//        hhdProject = json["hhd_project_id"].arrayObject!
//        super.init()
//        id = json["id"].int!
//    }
   
    func getObject(data: JSON) -> StockPicking {
        let stock = StockPicking()
        stock.name = data["name"].stringValue
        stock.state = data["state"].stringValue
        stock.hhdProject = data["hhd_project_id"].arrayObject
        stock.date = data["date"].stringValue
        stock.id = data["id"].intValue
        return stock
    }
    
    func convert(stringDate: String) -> String? {
        if date != nil {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            dateFormatter.locale = Locale(identifier: "en_US")
            let newDate = dateFormatter.date(from: stringDate)!
            dateFormatter.dateFormat = "dd/MM/yyyy"
            return dateFormatter.string(from: newDate)
        }
        return nil
    }
   
    override func getObjectName() -> String {
        return "stock.picking"
    }
}





