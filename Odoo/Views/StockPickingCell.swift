//
//  PhieuNhapChuyenCell.swift
//  Odoo
//
//  Created by gray buster on 9/13/18.
//  Copyright © 2018 gray buster. All rights reserved.
//

import UIKit

class StockPickingCell: UITableViewCell {

    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var projectLabel: UILabel!
    @IBOutlet weak var idLabel: UILabel!
    
    var stockPicking: StockPicking? {
        didSet {
            updateUI()
        }
    }
    
    private func updateUI() {
        guard let stockPicking = stockPicking else { return }
        idLabel.text = stockPicking.name
        if let hhdProject = stockPicking.hhdProject , let projectName = hhdProject.last as? String {
            let splitName = projectName.components(separatedBy: "]")
            projectLabel.text = splitName.last
        }else {
            projectLabel.text = "Không có"
        }
        let splitDate = stockPicking.date?.components(separatedBy: " ")
        if let date = stockPicking.convert(stringDate: splitDate!.first!) {
            dateLabel.text = date
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    
    }
}
